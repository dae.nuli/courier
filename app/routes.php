<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
 
// API Controller
Route::group(array('before'=>'apikey'),function(){
	Route::controller('/api/register/{key?}','ApiRegister');
});

Route::get('/','Home@index');
Route::get('/register','Home@register');
Route::get('/detail-register','Home@nextregister');
Route::post('/complete','Home@storenextregister');
Route::post('/register','Home@postRegister');
Route::get('/success','Home@success');
Route::get('/forgot','Home@forgot');
Route::post('/forgot','Home@postForgot');

Route::get('/hanyamoto/callback','Home@callback');
Route::get('/hanyamoto/winner','Home@getWinner');

Route::controller('login','HomeController');
Route::group(array('before'=>'auth_admin'),function(){

	Route::group(array('prefix' => 'admin'), function(){

		Route::get('/',function(){
			return Redirect::to('/admin/home');
		});
		Route::get('/home','Dashboard@index');
		Route::get('/notifications','Dashboard@notification');
		Route::post('/home','Dashboard@postIndex');
		Route::controller('/profile','Profile');
		//ACL
		Route::group(array('before' => 'acl'), function(){
			Route::controller('/about','About');
			Route::controller('/find-work','Project');
			Route::controller('/request','Requests');
			Route::controller('/messages','Messages');
			Route::controller('/users','Users');
			
		});
		Route::group(array('before' => 'acl_admin'), function(){

			// Route::controller('/deleted-staff','DeletedUsers');
			Route::controller('/group','Groups');
			Route::controller('/permission','Permissions');

		});
	});
	Route::get('/logout','Dashboard@logout');
	
});
// App::error(function($exception, $code) { 
// 	if(Request::is('admin/*')){
// 		switch ($code) {
// 			case 404:
// 				$errors = 'Page not found';
// 				$note   = 'We could not find the page you were looking for';
// 				break;
// 			default:
// 				$errors = 'Something went wrong';
// 				$note   = 'We will work on fixing that right away';
// 				break;
// 		}
// 		View::share('title',$code);
// 		View::share('path',$errors);
// 		$data['code']  = $code;
// 		$data['error'] = $errors;
// 		$data['note']  = $note;
// 		return View::make('backend.errors',$data);
// 	}
// });

App::error(function($exception, $code) { 
	if(Request::is('api*')){		
		$data['success'] = FALSE;
		$data['message'] = 'URL not valid';
		return  Response::json($data);
	}
});